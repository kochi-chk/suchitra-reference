# Document conversion

## Prerequisites

You should have the following installed on your system

* Python
* Pandoc
* Panflute

**Install Pandoc on Windows**

Download and run the latest [Pandoc installer](https://pandoc.org/installing.html){:target="_blank"}  

**Install Panflute**

``` python
pip install panflute
```


## Doc Conversions

### Docx to Markdown 

Docx to Markdown with images


1. Create a `remove_img_size.py` (Removes image size attributes in the markdown output while conversion).

    ``` python 
    import panflute as pf


    def change_md_link(elem, doc):
        if isinstance(elem, pf.Image):
            elem.attributes.pop('width', None)
            elem.attributes.pop('height', None)
        return elem


    if __name__ == "__main__":
        pf.run_filter(change_md_link)
    ```

2. To convert run the following command

    ``` python 
    pandoc --extract-media=./ inputFileName.docx -F remove_img_size.py -o outPutfileName.md
    ```

3. To remove linebreaks or paragraph breaks in the generated document, use the following tool.

    https://www.textfixer.com/tools/remove-line-breaks.php

4. Do other text sanitisation.

### DOCX to AsciiDoc

To convert DOCX to AsciiDoc, we first convert the DOCX file to markdown and then use a kramdown extension (Ruby) for converting the Markdown document to AsciiDoc.

#### Install Ruby on Windows

 1. Go to [Ruby downloads](https://rubyinstaller.org/downloads/){:target="_blank"} page. 
 
 2. Download and install the latest Ruby with Devkit [`Ruby+Devkit x.x.x-x (x64)`]. 

 2. Double-click the downloaded file and proceed through the wizard to install it.
 
 3. A command prompt opens after the installation wizard with the options to install a few more compenents. Install these components by typing the numbers `1`, `2`, `3` and pressing the **Enter** key one after the other.

#### Install the Kramdown AsciiDoc extension

 Run the following command.

``` ruby 
gem install kramdown-asciidoc
```

#### Conversion

1. Place the following in a single directory.
    1. [remove_img_size.py](remove_img_size.py){:target="_blank"}
    2. Source document (DOCX)  

2. To convert DOCX to markdown, run the following command.

    ``` bash 
    pandoc --wrap=none -s --extract-media=./ source-document.docx -F remove_img_size.py -o result-doc.md
    ```

3. To convert the Markdown document to AsciiDoc, run the following command.

    ``` bash 
    kramdoc result-doc.md
    ```

**Further Reference**: https://matthewsetter.com/technical-documentation/asciidoc/convert-markdown-to-asciidoc-with-kramdoc/

### Markdown to Docx

``` python 
pandoc --reference-doc twocolumns.docx -o UsersGuide18.docx example122.text
```
### DOCX to HTML

``` python
pandoc --extract-media=. -s word.docx -t html -c styles.css -o output.html
```
### HTML to Docx
``` python 
pandoc --reference-doc pandocTheme.docx -f html -t docx -o output.docx input.html
```

**Syntax Higlight**

1. Run the following command. Use any of the available themes such as `espresso`, `haddock`, `kate`, `monochrome`, `pygments`, `tango`	 and `zenburn`
``` python 
pandoc --print-highlight-style pygments > my_style.theme
```
2. Make changes to the generated `my_style.theme` file if required.

2. Run the following conversion command

    ``` python 
    pandoc --reference-doc=custom-reference.docx -f html -t docx -s --highlight-style my_style.theme -o output.docx input.html
    ```

### HTML to Markdown

``` script 
pandoc -s -r --extract-media html pmm.html -o output.md
```

``` script 
pandoc -s -r --extract-media html input.html -o output.text
```

### HTML to AsciiDoc

``` python 
pandoc -f html -t AsciiDoctor -o outpot.adoc input.html 
```

``` python 
pandoc -f html -t AsciiDoc -o output.asc input.html
```

### AsciiDoc to HTML

https://rmoff.net/2020/04/16/converting-from-AsciiDoc-to-google-docs-and-ms-word/

``` python 
AsciiDoctor --backend html5 -a data-uri input.adoc
```

### AsciiDoc to Docx
``` python 
INPUT_ADOC=my_input_file.adoc
AsciiDoctor --backend docbook --out-file - $INPUT_ADOC| \
pandoc --from docbook --to docx --output $INPUT_ADOC.docx

On the Mac, this will open the generated file in MS Word

open $INPUT_ADOC.docx
```
``` python 
AsciiDoctor --backend docbook --out-file - $INPUT_ADOC| \
pandoc --from docbook --to docx --output $INPUT_ADOC.docx \
       --highlight-style espresso
```

``` python 	   
AsciiDoctor --backend docbook --out-file - $INPUT_ADOC| \
pandoc --from docbook --to docx --output $INPUT_ADOC.docx \
       --highlight-style my.theme
```

### ASCII to Docbook

``` python
AsciiDoctor -a data-uri -a icons -b docbook wagetype_dev.adoc
```

### wkhtmltopdf
``` python
wkhtmltopdf http://127.0.0.1:4000/docs/CSharpCodingStandards.html google.pdf
```

### Weasyprint

``` python 
python -m weasyprint http://weasyprint.org weasyprint.pdf
```

## Promote heading levels in MS Word

To promote or demote heading levels in your Word document, follow the steps below.

1. Open your Word document

2. To open the VBA editor in Word, press the **Alt+ F11** keys on your keyboard.

    The VBA editor screen appears.

3. On the toolbar of the VBA editor, select **Insert** > **Module**.

4. In the new window that appears, insert the following code.

    ``` 
    Sub PromoteOrDemoteParagraph()
    Dim strRule As String
    
    Selection.WholeStory
    strRule = InputBox("To promote heading level, enter p" & vbNewLine & "To demote heading level, enter d")
    
    If strRule = "p" Then
        Selection.Paragraphs.OutlinePromote
    Else
        Selection.Paragraphs.OutlineDemote
    End If
    Selection.Collapse wdCollapseStart
    End Sub
    ```

5. Click the **Run** button or press the **F5** key on your keyboard.

6. In the dialog box that appears, type any of the following as required.

    <table id="if-then">
    <thead>
    <tr>
    <th>
    <p>IF</p>
    </th>
    <th>
    <p>Then</p>
    </th>
    </tr>
    </thead>
    <tbody>
    <tr>
    <td>
    <p>To promote the heading levels.</p>
    </td>
    <td>
    <p>Type <b><em>p.</em></b></p>
    </td>
    </tr>
    <tr>
    <td>
    <div>
    <div>To demote the heading levels.</div>
    </div>
    </td>
    <td>
    <p>Type <b><em>d</em></b>.</p>
    </td>
    </tr>
    </tbody>
    </table>


**Reference**: [3 Ways to Batch Promote or Demote Heading Levels in Your Word Document](https://www.datanumen.com/blogs/3-ways-batch-promote-demote-heading-levels-word-document/)
