# Technical Writing Resources

## Learning resources

1. [Udemy course](https://www.udemy.com/share/103MXc3@KQGiG9tuVKMZK7jRz4_SAaCaf9tL2Txj2i5sdI1aXokpwXFpwIVPr5ykMxq1gpc=/){:target="_blank"}

2. [Google Technical Writing One](https://developers.google.com/tech-writing/one){:target="_blank"}

3. [Google Technical Writing Two](https://developers.google.com/tech-writing/two){:target="_blank"}

4. **Micosoft Style Guide**:

    1. [Describing interactions with UI](https://docs.microsoft.com/en-us/style-guide/procedures-instructions/describing-interactions-with-ui){:target="_blank"}


    2. [Formatting text in instructions](https://docs.microsoft.com/en-us/style-guide/procedures-instructions/formatting-text-in-instructions#in-documentation-and-technical-content){:target="_blank"}

5. [Free Online Textbook for Technical Writing](https://www.prismnet.com/~hcexres/textbook/){:target="_blank"}

6. [Amruta Ranade: Writing Proposals: Key Takeaways for Season of Docs](https://www.notion.so/Writing-Proposals-Key-Takeaways-for-Season-of-Docs-c14a7ebb2fb84abc86d379b0a1cc3fc8){:target="_blank"}

1. [Amruta Ranade: How I Write Technical Documents (Part 2){:target="_blank"}](https://youtu.be/TXisTiNAWyE){:target="_blank"}

2. [Amruta Ranade Website](https://amrutaranade.com/category/tech-writing/){:target="_blank"}

3. [Amruta Ranade: Day in my life as a Technical Writer](https://youtu.be/8AbBf3j0hSY){:target="_blank"}

4. [Amruta Ranade: Skillshare- Technical Writing Process for Beginners](https://www.skillshare.com/site/join?teacherRef=8662963&via=teacher-referral&utm_campaign=teacher-referral&utm_source=Twitter&utm_medium=teacher-referral&t=Tech-Writing-for-Non-Writers&sku=454456196){:target="_blank"}

5. [6 Inspiring Technical Writing Portfolios (+ my new portfolio){:target="_blank"}](https://youtu.be/crePAmhdpww){:target="_blank"}

6. [Technical Writer HQ - Tutorial](https://technicalwriterhq.com/technical-writing/){:target="_blank"}


## Technical writing style guides

**Articles on style guides**

1. [Technical Writer Style Guide Examples - Technical Writer HQ ](https://technicalwriterhq.com/technical-writer-style-guide/){:target="_blank"}

1. [Using a Style Guide for Technical Writing](https://instrktiv.com/en/style-guide-technical-writing/){:target="_blank"}

**List Of Guides**

1. [DigitalOcean's Technical Writing Guidelines](https://www.digitalocean.com/community/tutorials/digitalocean-s-technical-writing-guidelines){:target="_blank"}

1. [Polaris Product content guidelines](https://polaris.shopify.com/foundations/content/product-content#navigation){:target="_blank"}

1. [Salesforce Style Guide for Documentation and User Interface Text](https://developer.salesforce.com/docs/atlas.en-us.salesforce_pubs_style_guide.meta/salesforce_pubs_style_guide/overview.htm){:target="_blank"}

1. [Style guide for NetApp docs](https://docs.netapp.com/us-en/contribute/style.html#write-conversationally){:target="_blank"}

1. [SUSE Documentation Style Guide](https://documentation.suse.com/en-us/style/current/single-html/docu_styleguide/){:target="_blank"}

1. [Institute Of Engineering and Technology](https://www.theiet.org/media/5182/technical-report-writing.pdf){:target="_blank"}

1. [Google developer documentation style guide](https://developers.google.com/style){:target="_blank"}

1. [IBM: Modular Documentation Reference Guide](https://redhat-documentation.github.io/modular-docs/){:target="_blank"}: Guidance for all things connected to modular documentation, including implementing those guidelines in AsciiDoc.

1. [IBM: AsciiDoc Mark-up Quick Reference](https://redhat-documentation.github.io/asciidoc-markup-conventions/){:target="_blank"}: Guidance specific to writing in AsciiDoc. Includes links to complete documentation for AsciiDoc and Asciidoctor.

1. **Rackspace Style Guide**
   
    - [Rackspace Style Guide for Technical Content](https://docs.rackspace.com/docs/style-guide/){:target="_blank"}

    - [Introductory text](https://docs.rackspace.com/docs/style-guide/style/lists#introductory-text){:target="_blank"} 

    - [Gender neutral pronouns](https://docs.rackspace.com/docs/style-guide/writing/use-gender-neutral-pronouns){:target="_blank"}

    - [Salesforce Developer](https://developer.salesforce.com/docs/atlas.en-us.196.0.salesforce_pubs_style_guide.meta/salesforce_pubs_style_guide/text_guidelines.htm#UITextToneTitle){:target="_blank"}

## Plain Language and Simplified Technical English

1. [plainlanguage.gov](https://www.plainlanguage.gov/){:target="_blank"}

1. [Federal plain language guidelines](https://www.plainlanguage.gov/guidelines/){:target="_blank"}

1. [Plain language - resources](https://www.plainlanguage.gov/resources/){:target="_blank"}

1. [Mary Dash’s Writing Tips- Plain language](https://www.plainlanguage.gov/resources/articles/dash-writing-tips/){:target="_blank"}

1. [DOD Writing Style Guide PDF](https://www.esd.whs.mil/Portals/54/Documents/DD/iss_process/Writing_Style_Guide.pdf?ver=2018-06-25-144030-870){:target="_blank"}
   
1. [design.va.gov Style guide](https://design.va.gov/content-style-guide/error-messages/){:target="_blank"}

1. [Conscious style guide](https://consciousstyleguide.com/plain-language/){:target="_blank"}

1. [writer.com: Plain Language](https://writer.com/guides/plain-language/){:target="_blank"}

1. [Service experience and digital delivery](https://www2.gov.bc.ca/gov/content/governments/services-for-government/service-experience-digital-delivery/web-content-development-guides/web-style-guide/writing-guide/plain-language){:target="_blank"}

1. [Federal register](https://www.archives.gov/federal-register/write/plain-language){:target="_blank"}

1. [archives.gov/open/plain-writing](https://www.archives.gov/open/plain-writing){:target="_blank"}



## UX Writing style guides

1. [Adobe Spectrum](https://spectrum.adobe.com/page/grammar-and-mechanics/){:target="_blank"}

1. [Material Design](https://material.io/design/communication/writing.html){:target="_blank"}

1. [Intuit](https://material.io/design/communication/writing.html){:target="_blank"}

1. [nngroup.com](https://www.nngroup.com){:target="_blank"}
    - [Progressive disclosure](https://www.nngroup.com/articles/progressive-disclosure/){:target="_blank"}
    - [Ephemeral web-based applications](https://www.nngroup.com/articles/ephemeral-web-based-applications/){:target="_blank"}

1. [Adobe XD](https://xd.adobe.com/ideas/process/information-architecture/ux-writing-guidelines/#:~:text=UX%20writing%20needs%20to%20be,such%20terms%20with%20simple%20words.){:target="_blank"}

1. https://developers.google.com/style/inclusive-documentation  

1. https://xd.adobe.com/ideas/process/information-architecture/information-ux-architect/  

1. https://xd.adobe.com/ideas/process/information-architecture/  

1. https://docs.microsoft.com/en-us/style-guide/numbers   

1. https://docs.microsoft.com/en-us/style-guide/a-z-word-list-term-collections/term-collections/computer-device-terms   

1. https://docs.microsoft.com/en-us/style-guide/a-z-word-list-term-collections/term-collections/computer-device-terms  
    
1. https://docs.microsoft.com/en-us/style-guide/bias-free-communication   

1. https://community.adobe.com/t5/robohelp-discussions/multiple-css-files-in-a-project/m-p/12564048   

1. https://spectrum.adobe.com/page/writing-about-people/#Writing-about-gender-and-sexuality   

1. https://spectrum.adobe.com/page/platform-scale/#-Choosing-the-correct-scale   

1. https://spectrum.adobe.com/page/in-product-word-list/  

## Information mapping

**informationmapping.com**

* [Present your information clearly using tables](https://support.informationmapping.com/hc/en-us/articles/201635581-3-Present-your-information-clearly-using-tables){:target="_blank"}{target=_blank}

    Explanation of the following six information categories or information types and where, when, and how to use them.
      
    - Process
    - Procedure
    - Principle
    - Concept
    - Structure
    - Fact

* [Structure your content using Maps and Blocks](https://support.informationmapping.com/hc/en-us/articles/203523982-Inserting-Information-Type-Blocks){:target="_blank"}{target=_blank}

* [Properly add a large image or flowchart](https://support.informationmapping.com/hc/en-us/articles/360004116457-How-can-I-properly-add-a-large-image-or-flowchart-){:target="_blank"}{target=_blank}

* [Manage larger documents using Overview Maps](https://support.informationmapping.com/hc/en-us/articles/201635611-4-Manage-larger-documents-using-Overview-Maps){:target="_blank"}{target=_blank}

* [Structure your content using Maps and Blocks](https://support.informationmapping.com/hc/en-us/articles/201509152-2-Structure-your-content-using-Maps-and-Blocks){:target="_blank"}


## DITA

* [Learning DITA](https://learningdita.com/){:target="_blank"}
  
* [Oxygen DITA Style Guide](https://www.oxygenxml.com/dita/styleguide/Authoring_Concepts/c_About_the_Style_Guide.html){:target="_blank"}


##


