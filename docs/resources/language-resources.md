# Language Resources

## The Writing Center - University Of Texas

### Writing Resources

https://www.utsa.edu/twc/Resources.html#modules

https://www.utsa.edu/twc/Modules/Essays_Module_1/Essays_Module.html

### Library Resources For ESL / International Students

https://libguides.utsa.edu/c.php?g=524180&p=3583448

## Excelsior College Home

https://owl.excelsior.edu/grammar-essentials/parts-of-speech/nouns/proper-nouns/

* The Writing Process
* Research
* Citation & Documentation
* Rhetorical Styles
* Argument & Critical Thinking
* Online Writing & Presentations
* Grammar Essentials
* Avoiding Plagiarism
* Academic Writing 101
* Writing in the Disciplines
* Writing Refresher

## VoiceTube

 https://www.voicetube.com/channels/learning

## Open Education Database

[50 Essential Resources for ESL Students](https://oedb.org/ilibrarian/50_essential_resources_for_esl_students/)

## Daves ESL Cafe
https://www.eslcafe.com/resources/grammar-lessons

## Breaking News English

[PRINT, LISTEN, NEWS, GRAMMAR](https://breakingnewsenglish.com/)

## BBC Learning English

* https://www.bbc.co.uk/learningenglish/english/course/intermediate

* [BBC English at Work](https://www.bbc.co.uk/learningenglish/english/features/english-at-work)

## St. Johns River

Crash Course Series
https://learningresources.sjrstate.edu/CrashCourseSeries

Crash Course on Chicago Style Citations
https://learningresources.sjrstate.edu/CrashCourseSeriesonFormattingandCitations/CHICAGO

Math: https://learningresources.sjrstate.edu/CrashCourseSeriesinMath/Fractions


## YouTube

* [Guys With Games ESL  | Parts of Speech | (GWG) part 1/20](https://youtu.be/R9D8BZYZe4Y)

* [Dr Lakshmi S - English Lessons](https://youtu.be/nhDXn-gZlaM)

* [Marielyn Correa](https://youtu.be/g4pAwb0aKBs)

* [Learn Easy Englishs](https://youtu.be/0l69KEx7GQo)  
  https://ilearneasy.co.uk/

* 


