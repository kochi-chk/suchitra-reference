
# Links for reference
  
## Solving PM challenges with DocOps and Agile technical writing

### Technical writing in an Agile world**  

https://www.liquidplanner.com/blog/the-docops-trend-applying-agile-and-devops-to-technical-documentation/

## DocOps – How CA Technologies Got Started

https://www.linkedin.com/pulse/20140911020510-10335988-the-ca-technologies-docops-platform-phase-1?trk=mp-reader-card 

## Basic principles of DocOps

[Docs-as-Code promotes quality in documentation](https://pronovix.com/blog/how-improve-docops-using-cicd-and-docs-code)

 - https://www.ibm.com/docs/en/iis/8.1?topic=console-configuring-your-web-browser-work-web  
 - https://docs.microsoft.com/en-us/windows/security/identity-protection/hello-for-business/hello-biometrics-in-enterprise
 - https://docs.servicenow.com/en-US/bundle/tokyo-it-business-management/page/product/agile-development/concept/agile-development.html
 - https://buildfire.com/mobile-app-user-stories/
 - https://propelrr.com/blog/user-story-examples-fintech-apps
 - https://propelrr.com/blog/how-to-write-user-stories-mobile-apps
 - https://www.oracle.com/scm/product-lifecycle-management/technical-resources/documentation/agile/#plmprocess
 - https://www.oracle.com/technical-resources/documentation/agile-eseries.html
 - https://www.krasamo.com/agile-development-process/
 - https://documentation.divio.com/introduction/
 - https://lisk.com/blog/announcement/lisk-documentation-migrated-asciidoc-and-antora
 - https://marina-hernandez.com/2020/11/29/developer-docs-using-antora-to-create-a-unified-output-with-content-from-different-repos/
 - https://docs-as-co.de/
 - https://blog.jetbrains.com/writerside/2022/01/writing-with-style-10-style-guides-to-inspire-you/
 - https://rmoff.net/2020/04/16/converting-from-asciidoc-to-google-docs-and-ms-word/
 - https://intellij-asciidoc-plugin.ahus1.de/docs/users-guide/features/advanced/antora.html
 - https://plugins.jetbrains.com/plugin/16136-grazie-professional/docs
 - https://learn.microsoft.com/en-us/dynamics365/customer-service/csw-overview
 - https://netsuitedocumentation1.gitlab.io/netsuitedocumentation1/preface_3710621755.html
 - https://docs.oracle.com/en/cloud/saas/netsuite/ns-online-help/preface_3710621755.html
 - https://benjam.info/panam/
 - https://danrh.net/a/pandoc/2022/02/27/pandoc-md-to-styled-pdf.html
 - https://docs.aspose.com/words/python-net/hello-world/
 - https://danrh.net/a/pandoc/2022/02/27/pandoc-md-to-styled-pdf.html
  

## Microsft Code With Engineering Playbook
 
 Microsft's Code With Engineering Playbook is a great resource to learn about the best practices and guidelines on development, testing, CI/CD, etc

- https://microsoft.github.io/code-with-engineering-playbook/

- https://microsoft.github.io/code-with-engineering-playbook/user-interface-engineering/#general-guidance

- https://microsoft.github.io/code-with-engineering-playbook/agile-development/advanced-topics/backlog-management/ 

- https://microsoft.github.io/code-with-engineering-playbook/agile-development/advanced-topics/collaboration/ 

- https://microsoft.github.io/code-with-engineering-playbook/agile-development/advanced-topics/effective-organization/ 

- https://microsoft.github.io/code-with-engineering-playbook/agile-development/advanced-topics/team-agreements/

- https://microsoft.github.io/code-with-engineering-playbook/user-interface-engineering/

- https://microsoft.github.io/code-with-engineering-playbook/user-interface-engineering/#general-guidance


## Spotify Backstage, Agile, and TechDocs

- [Agile Coaching (AC) at Spotify: Shining a Light on the AC Career Framework](https://engineering.atspotify.com/2023/01/agile-coaching-ac-at-spotify-shining-a-light-on-the-ac-career-framework/)

- [Backstage.io](https://backstage.io/)

- [TechDocs Documentation](https://backstage.io/docs/features/techdocs/techdocs-overview)