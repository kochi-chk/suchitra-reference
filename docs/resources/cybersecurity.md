# Cybersecurity learning resources


# Robert Lackey's Linkedin Post

Following are from [Robert Lackey's Linkedin Post](https://www.linkedin.com/posts/activity-6954459901066711040-gtkg/?utm_source=linkedin_share&utm_medium=android_app){:target="_blank"}


If you are new to tech and trying to transition into cybersecurity or some other areas of tech, you shouldn’t need to pay for any type of training. Unless the topic is very complex, all you need is a decent laptop and an internet connection.

There are plenty of free videos and resources online, and I've listed some free resources below to help get you started.

## Networking 

[Computer Networking Course - Network Engineering [CompTIA Network+ Exam Prep]](https://youtu.be/qiQR5rTSshw){:target="_blank"}

[More Networking Youtube Playlist](https://www.youtube.com/playlist?list=PLG49S3nxzAnksQpejrRxNZoRSo0pcKXkG){:target="_blank"}

[FREE CCNA 200-301 // Complete Course // NetworkChuck 2021](https://www.youtube.com/playlist?list=PLIhvC56v63IJVXv0GJcl9vO5Z6znCVb1P){:target="_blank"} 


## Linux
Linux - https://lnkd.in/g7KJBUYd

More Linux -  https://lnkd.in/gUK8PU4p

Linux operating systems

https://lnkd.in/g2M__A5n

https://lnkd.in/gyc4R_F7

https://lnkd.in/gSiHYRNg

https://lnkd.in/g5GsUT7H

## Windows Server

Windows Server - https://lnkd.in/gWUTmN-5

More Windows Server- https://lnkd.in/gsWZQnwj

Microsoft Operating Systems
https://lnkd.in/gP3nxKpZ

## Programming languages

Python - https://lnkd.in/g_NpsqEM

Golang - https://lnkd.in/gmwz4ed5

## List

Huge list of computer science resources (This one is great! Some links might not work, but I'm sure you can find them by doing a quick search) - https://lnkd.in/gQvxbypj

Databases - https://lnkd.in/gWQmYwib

CompTIA Security+ - https://lnkd.in/gyFy_CG9

CISSP - https://lnkd.in/gUFjihpJ

Penetration testing - https://lnkd.in/gAdgyY6h

Web application testing - https://lnkd.in/g5FkXWej

Weekly HackTheBox series and other hacking videos - https://lnkd.in/gztivT-D

Resources for practicing what you learned:

Network simulation software
https://lnkd.in/gRMak7_x 

Virtualization software
https://lnkd.in/gFkyFVvF



Capture the flag
https://lnkd.in/gpnYs5Qj

https://www.vulnhub.com/

https://lnkd.in/gn2AEYhw

https://lnkd.in/g5FkXWej
