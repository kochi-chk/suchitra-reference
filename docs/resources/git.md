# Git commands

## Clone a specific branch 

``` py title="Clone specific branch"
git clone --branch branchName repoURL
```

``` bash title="Clone specific branch"
git clone -b branchName repoURL
```

