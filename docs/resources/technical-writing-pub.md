# Technical Writing - Journals and Publications

See below a list of technical writing journals and publication.

[Technical Communication (Published by STC)](https://www.stc.org/techcomm/)

[Technical Communication Quarterly (Published by ATTW)](https://www.tandfonline.com/toc/htcq20/current)

[IEEE Transactions in Professional Communication (Published by the Professional Communication Society of the Institute of Electrical and Electronics Engineers (IEEE))](https://ieeexplore.ieee.org/xpl/RecentIssue.jsp?reload=true&punumber=47)

[Journal of Business and Technical Communication: SAGE Journals](https://journals.sagepub.com/toc/JTW/current)

**Reference**: https://amrutaranade.com/category/ms-in-tech-comm/



