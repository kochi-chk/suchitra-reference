# Install Antora
??? note

    ??? note
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.


Refer to the [Antora Installation guide](`https://docs.antora.org/antora/2.3/install-and-run-quickstart/`)

## Antora Lunr

Refer to the [Antora Lunr guide](`https://github.com/Mogztter/antora-lunr#readme" target="_blank"`).

## Install http server

```python
npm i -g http-server
OR
yarn global add http-server
```

## asciidoctor pdf

```python
npm i -g @asciidoctor/core asciidoctor-pdf
```

## NPM clean install

```python
npm run clean-install
```

```python
npm run build
```

```python
# Starter pipeline
# Start with a minimal pipeline that you can customize to fit your needs.
# Add steps that build, run tests, deploy, and more:
# https://aka.ms/yaml

name: Build and Publish Antora Site

trigger:
  branches:
    include:
      - main

pool:
  vmImage: 'Ubuntu-20.04'

steps:
- task: NodeTool@0
  inputs:
    versionSpec: '14.x'

- script: |
    npm install
    npx antora --fetch --cache-dir .cache/antora --attribute page-pagination= --redirect-facility gitlab --to-dir public antora-playbook.yml
  displayName: 'Generate Antora site'

- task: PublishBuildArtifacts@1
  inputs:
    PathtoPublish: public
    ArtifactName: build
    publishLocation: Container

```

```python
site:
  title: Antora Demo Site
  # the 404 page and sitemap files only get generated when the url property is set
  url: https://adarashbm11.gitlab.io/demo/docs-site
  start_page: component-b::index.adoc


content:
  sources:
  - url: https://gitlab.com/antora/demo/demo-component-a.git
    branches: HEAD
    # setting edit_url to false disables the Edit this Page link for any page that originates from this repository
    # the same thing can be achieved by adding empty credentials (i.e., @) in front of the domain in the URL
    edit_url: false
  - url: https://gitlab.com/antora/demo/demo-component-b.git
    branches: [main, v2.0, v1.0]
    start_path: docs



asciidoc:
  attributes:
    experimental: ''
    idprefix: ''
    idseparator: '-'
    page-pagination: ''

ui:
  bundle:
    url: https://gitlab.com/antora/antora-ui-default/-/jobs/artifacts/HEAD/raw/build/ui-bundle.zip?job=bundle-stable
    snapshot: true
```

```python
site:
  title: Antora Demo Site
  # the 404 page and sitemap files only get generated when the url property is set
  url: https://antora-project-docs-site.azurewebsites.net
  start_page: component-b::index.adoc


content:
  sources:
  - url: https://gitlab.com/antora/demo/demo-component-a.git
    branches: HEAD
    # setting edit_url to false disables the Edit this Page link for any page that originates from this repository
    # the same thing can be achieved by adding empty credentials (i.e., @) in front of the domain in the URL
    edit_url: false
  - url: https://gitlab.com/antora/demo/demo-component-b.git
    branches: [main, v2.0, v1.0]
    start_path: docs



asciidoc:
  attributes:
    experimental: ''
    idprefix: ''
    idseparator: '-'
    page-pagination: ''

ui:
  bundle:
    url: https://gitlab.com/antora/antora-ui-default/-/jobs/artifacts/HEAD/raw/build/ui-bundle.zip?job=bundle-stable
    snapshot: true
```
