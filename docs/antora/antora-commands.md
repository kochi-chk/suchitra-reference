# Antora Commands

``` java 
npx antora --fetch antora-playbook.yml

npx antora local-antora-playbook.yml

http-server build/site -c-1
```

``` java 
set "LIVERELOAD=true" && gulp serve

set "LIVERELOAD=true" && npx gulp

set "LIVERELOAD=true" && npx gulp serve

npx gulp serve

set "LIVERELOAD=true" && gulp preview
```

``` java 
set "LIVERELOAD=true" -and npx gulp
```

``` java 
pandoc --extract-media=. LearningDevelopmentCompleteUG.docx -o lnd.html
```

## Antora fetch (Depreciated)

``` java 
antora antora-playbook.yml
antora --fetch antora-playbook.yml
antora --fetch local-playbook.yml 
antora local-playbook.yml 
```

## Antora fetch local lunr (Depreciated)


``` java linenums="1"
antora-playbook.yml
set "DOCSEARCH_ENABLED=true" && set "DOCSEARCH_ENGINE=lunr" && antora --generator antora-site-generator-lunr antora-playbook.yml

local-playbook.yml
set "DOCSEARCH_ENABLED=true" && set "DOCSEARCH_ENGINE=lunr" && antora --generator antora-site-generator-lunr local-playbook.yml
```

## Serve

``` asciidoc linenums="1"
http-server build/site -c-1
http-server build/site -c-1 -p 5000
-c-1 flag to disable caching
``` 

``` asciidoc 
http-server build/site -c-1 -p 5000 DDOCSEARCH_ENABLED=true DOCSEARCH_ENGINE=lunr antora site.yml
```