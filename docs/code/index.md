# Site elements

## Expand/ collapse button

=== "HTML"

    ```py
    <button class="fcc-btn" id="expandCollapseBtn" onclick="expandCollapseAll()">Expand All  <span>(+)</span></button>
    ```

=== "CSS"

    ```css
    .fcc-btn {
      background: none !important;
      border: none;
      padding: 20px 0px 2px 0px !important;
      font-family: "Roboto";
      font-size: 1rem;
      color: #2054ee;
      text-decoration: underline;
      cursor: pointer;
    }
    ```

=== "Script"

    ```JavaScript
    let isExpanded = false;
    const expandCollapseAll = () => {
    let detailsElements = document.querySelectorAll("details");
    detailsElements = [...detailsElements];
    if (!isExpanded) {
      detailsElements.map((item) => item.setAttribute("open", true));
      document.getElementById("expandCollapseBtn").innerHTML =
          "Collapse All" + "&#160;(&#8722;)";
      } else {
      detailsElements.map((item) => item.removeAttribute("open"));
        document.getElementById("expandCollapseBtn").innerHTML =
          "Expand All" + "&#160;(&#x2B;)";
      }
    isExpanded = !isExpanded;
    };
    ```

## Admonitions

=== "HTML Important"

    ```html
    <div class="admonitionblock important">
      <table style="width: 90%;">
        <tbody>
          <tr>
            <td class="icon">
              <i class="fa icon-important" title="IMPORTANT"></i>
            </td>
            <td class="content">
              <p>
                Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean
                commodo. Aenean massa. Cum sociis natoque penatibus et magnis dis
                parturient montes. In enim justo, rhoncus ut, imperdiet a, venenatis
                vitae, justo. Nullam dictum. Integer tincidunt. Cras dapibus. Aenean
                vulputate eleifend tellus.<em>Product</em>. Aenean leo ligula,
                eleifend ac, enim.
              </p>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    ```

=== "HTML Notes"

    ```html
    <div class="admonitionblock note">
      <table style="width: 90%;">
        <tbody>
          <tr>
            <td class="icon">
              <i class="fa icon-note" title="NOTE"></i>
            </td>
            <td class="content">
              <p>
                Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean
                commodo. Aenean massa. Cum sociis natoque penatibus et magnis dis
                parturient montes. In enim justo, rhoncus ut, imperdiet a, venenatis
                vitae, justo. Nullam dictum. Integer tincidunt. Cras dapibus. Aenean
                vulputate eleifend tellus.<em>Product</em>. Aenean leo ligula,
                eleifend ac, enim.
              </p>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    ```


=== "CSS"

    ```css
    div.admonitionblock {
      margin: 1.4rem 0 0;
    }
    div.admonitionblock p,
    div.admonitionblock td.content,
    div.admonitionblock td.content {
      font-size: calc(17 / var(--rem-base) * 1rem);
    }
    div.admonitionblock td.content > :not(.title):first-child,
    td.content > :not(.title):first-child,
    div.admonitionblock td.content > .title + *,
    div.admonitionblock td.content > .title + * {
      margin-top: 0;
    }
    div.admonitionblock pre,
    pre {
      font-size: calc(15 / var(--rem-base) * 1rem);
    }
    div.admonitionblock > table,
    > table {
      table-layout: fixed;
      position: relative;
      width: 100%;
    }
    div.admonitionblock td.content,
    td.content {
      padding: 1rem 1rem 0.75rem;
      background: #fafafa;
      width: 100%;
      word-wrap: anywhere;
    }
    div.admonitionblock .icon,
    .icon {
      position: absolute;
      top: 0;
      left: 0;
      font-size: calc(15 / var(--rem-base) * 1rem);
      padding: 0 0.5rem;
      height: 1.25rem;
      line-height: 1;
      font-weight: var(--admonition-label-font-weight);
      text-transform: uppercase;
      border-radius: 0.45rem;
      transform: translate(-0.5rem, -50%);
    }
    div.admonitionblock.caution .icon,
    .caution .icon {
      background-color: var(--caution-color);
      color: #fff;
    }
    div.admonitionblock.important .icon,
    .important .icon {
      background-color: var(--important-color);
      color: #fff;
    }
    div.admonitionblock.note .icon,
    .note .icon {
      background-color: var(--note-color);
      color: #fff;
    }
    div.admonitionblock.tip .icon,
    .tip .icon {
      background-color: var(--tip-color);
      color: #fff;
    }
    div.admonitionblock.warning .icon,
    .warning .icon {
      background-color: var(--warning-color);
      color: #fff;
    }
    div.admonitionblock .icon i,
    .icon i {
      display: inline-flex;
      align-items: center;
      height: 100%;
    }
    div.admonitionblock .icon i::after,
    .icon i::after {
      content: attr(title);
      font-size: 0.725rem;
      font-family: "Segoe UI", Arial, sans-serif;
      font-weight: 600;
    }
    ```

## Summary and notes

=== "HTML"

    ```html
    <div class="summary">
      <p>
        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo
        ligula. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient
        montes, nascetur. Donec quam felis, ultricies nec, pellentesque eu, pretium
        quis, sem.
      </p>
    </div>
    ```

=== "CSS"

    ```css
    div.summary {
      font-size: 102%;
      margin: 20px 0 20px 0;
      border-left: 5px solid #1b3fa5;
      border-radius: 5px;
      padding-left: 10px;
    }
    div.summary:before {
      content: "Summary: ";
      font-weight: bold;
      color: #404040;
      font-size: 15px;
    }
    ```

## Refresh page on load


```JavaScript
    if (window.location.href.indexOf('reload') == -1) {
      window.location.replace(window.location.href + '?reload');
    }
```

## Kendo UI Grid (HTML Table)

=== "Assets"

    ```html
    <link
      rel="stylesheet"
      href="https://kendo.cdn.telerik.com/2023.1.117/styles/kendo.default-ocean-blue.min.css"
    />
    <script src="https://kendo.cdn.telerik.com/2023.1.117/js/jquery.min.js"></script>
    <script src="https://kendo.cdn.telerik.com/2023.1.117/js/kendo.all.min.js"></script>
    ```

=== "HTML (Table)"

    ```html
    </table><table class="kendoTable" id="grid">
      <colgroup>
          <col style="width:20%" />
          <col style="width:30%" />
          <col style="width:50%" />
      </colgroup>
      <thead>
          <tr>
            <th>Key</th>
            <th>Values</th>
            <th>Description</th>
          </tr>
      </thead>
      <tbody>
          <tr>
            <td><code>DEMO_DEMO_DEMO</code></td>
            <td>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo.</td>
            <td> </td>
          </tr>
          <tr>
            <td><code>DEMO2_DEMO2_DEMO2</code></td>
            <td>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo.</td>
            <td> </td>
          </tr>
      </tbody>
    </table>
    ```

=== "Script"

    ```JavaScript
    $(document).ready(function() {
      $("#grid").kendoGrid({
        height: 550,
        sortable: true,
        search: true,
        filterable: {
        multi: true,
        search: true
        }
        });
      });
    ```


## Tabbed content 

[squidfunk.github.io](https://squidfunk.github.io/mkdocs-material/setup/extensions/python-markdown-extensions/#tabbed)
